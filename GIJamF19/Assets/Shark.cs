﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shark : MonoBehaviour
{
	[SerializeField]
	private Text instructionsText;
	[SerializeField]
	private Text timerText;
	[SerializeField]
	private GameObject otherPlayer = null;
	[SerializeField]
	private Image opponentIndicator;
	[SerializeField]
	private float speedLimit = 0.01f;

	[Header("Controls")]
	[SerializeField]
	private KeyCode up = KeyCode.W;
	[SerializeField]
	private KeyCode down = KeyCode.S;
	[SerializeField]
	private KeyCode left = KeyCode.A;
	[SerializeField]
	private KeyCode right = KeyCode.D;
	[SerializeField]
	private KeyCode brake = KeyCode.Space;
	[SerializeField]
	private KeyCode reset = KeyCode.R;

	[Header("Physics Constants")]
	[SerializeField]
	private float pushStrength = 1f;
	[SerializeField]
	private float spinStrength = 1f;
	[SerializeField]
	private float tiltAngle = 15f;
	[SerializeField]
	private float defaultDrag = 0.1f;
	[SerializeField]
	private float defaultAngularDrag = 0.9f;
	[SerializeField]
	private float brakeDrag = 0.9f;
	[SerializeField]
	private float brakeAngularDrag = 0.9f;

	[Header("Audio System")]
	[SerializeField]
	private AudioClip gameOverClip;
	[System.Serializable]
	private struct AudioThreshold
	{
		[SerializeField]
		public float distance;
		[SerializeField]
		public AudioClip clip;
	}
	[SerializeField]
	private AudioThreshold[] audioThresholds;

	// Internal variables, don't fuck with these
	private Rigidbody rb;
	private AudioSource audioSource;
	private Shark otherShark;
	private Camera cam;
	public bool readyToStart = true;
	private bool gameStarted = false;
	private bool gameOver = false;
	private Vector3 startPos;
	private Quaternion startRot;
	private float deathTimer;

	// Start is called before the first frame update
	void Start()
	{
		rb = GetComponent<Rigidbody>();
		audioSource = GetComponentInChildren<AudioSource>();
		cam = GetComponentInChildren<Camera>();
		otherShark = otherPlayer.GetComponent<Shark>();
		startPos = transform.position;
		startRot = transform.rotation;

		Reset();
    }

	// Update is called once per frame
	void Update()
	{
		if (!gameStarted)
		{
			if (Input.GetKeyDown(brake))
			{
				readyToStart = true;
				instructionsText.text = "Waiting for opponent...";
			}
			else if (readyToStart && otherShark.readyToStart)
			{
				gameStarted = true;
				instructionsText.text = "";
			}

			return;
		}

		DrawOpponentIndicator();

		if (!gameOver && gameStarted)
		{
			if (audioSource != null)
			{
				AdjustMusic();
			}

			// Keep moving
			UpdateDeathClock();
			if (rb.velocity.magnitude < speedLimit)
			{
				deathTimer -= Time.deltaTime;
				timerText.text += "\nSwim faster!";
			}

			if (deathTimer < 0f)
			{
				Lose();
				otherShark.Win();
			}
		}

		if (gameOver && Input.GetKeyDown(reset))
		{
			Reset();
		}
	}

	void FixedUpdate()
    {
		if (!gameOver && gameStarted)
		{
			ApplyMovement();
		}
	}

	void ApplyMovement()
	{
		//Movement
		if (Input.GetKey(up))
		{
			// Tilt up
			var tiltUpQuaternion = Quaternion.Euler(rb.rotation.eulerAngles.x, rb.rotation.eulerAngles.y, tiltAngle);
			rb.MoveRotation(tiltUpQuaternion);
		}
		else if (Input.GetKey(down))
		{
			// Tilt down
			var tiltDownQuaternion = Quaternion.Euler(rb.rotation.eulerAngles.x, rb.rotation.eulerAngles.y, -tiltAngle);
			rb.MoveRotation(tiltDownQuaternion);
		}
		else
		{
			// No tilt
			var tiltFlatQuaternion = Quaternion.Euler(rb.rotation.eulerAngles.x, rb.rotation.eulerAngles.y, 0f);
			rb.MoveRotation(tiltFlatQuaternion);
		}

		if (Input.GetKeyDown(left))
		{
			// Apply push forward
			rb.AddRelativeForce(pushStrength, 0f, 0f);
			// Turn left a bit
			rb.AddRelativeTorque(0f, -spinStrength, 0f);
		}
		if (Input.GetKeyDown(right))
		{
			// Apply push forward
			rb.AddRelativeForce(pushStrength, 0f, 0f);
			// Turn right a bit
			rb.AddRelativeTorque(0f, spinStrength, 0f);
		}

		// Braking
		if (Input.GetKeyDown(brake))
		{
			rb.drag = brakeDrag;
			rb.angularDrag = brakeAngularDrag;
		}
		if (!Input.GetKey(brake))
		{
			rb.drag = defaultDrag;
			rb.angularDrag = defaultAngularDrag;
		}
	}

	void AdjustMusic()
	{
		// Get distance between players
		Vector3 distanceVector = transform.position - otherPlayer.transform.position;
		float distanceToOtherPlayer = Mathf.Abs(distanceVector.magnitude);

		if (!audioSource.isPlaying)
		{
			foreach (AudioThreshold threshold in audioThresholds)
			{
				if (threshold.distance < distanceToOtherPlayer)
				{
					audioSource.PlayOneShot(threshold.clip);
					break;
				}
			}
		}
		float volumeFactor = distanceToOtherPlayer / audioThresholds[0].distance;
		if (volumeFactor > 1f)
		{
			volumeFactor = 1f;
		}
		audioSource.volume = Mathf.Lerp(1f, 0.5f, volumeFactor);
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (gameOver || !gameStarted)
		{
			return;
		}
		ContactPoint[] contacts = new ContactPoint[collision.contactCount];
		collision.GetContacts(contacts);
		foreach (ContactPoint cp in contacts)
		{
			if (cp.thisCollider.name == "Wall" || cp.otherCollider.name == "Wall")
			{
				continue;
			}
			if (cp.thisCollider.name == "Nose" && cp.otherCollider.name != "Nose")
			{
				Win();
				break;
			}
			if (cp.thisCollider.name != "Nose" && cp.otherCollider.name == "Nose")
			{
				Lose();
				break;
			}
		}
	}

	private void Reset()
	{
		rb.velocity = Vector3.zero;
		rb.angularVelocity = Vector3.zero;
		transform.position = startPos;
		transform.rotation = startRot;
		opponentIndicator.enabled = false;
		deathTimer = 30f;
		UpdateDeathClock();
		if (audioSource != null)
		{
			audioSource.Stop();
		}
		instructionsText.fontSize = 36;
		instructionsText.text = "Boop the snoot!";
		instructionsText.text += "\nMash " + left + " to turn left. Mash " + right + " to turn right.";
		instructionsText.text += "\nAlternate " + left + " and " + right + " to move forwards.";
		instructionsText.text += "\nHold " + up + " to tilt upwards. Hold " + down + " to tilt downwards.";
		instructionsText.text += "\nPress " + brake + " to slow down.";
		instructionsText.text += "\nYour goal is to boop your opponent with your nose.";
		instructionsText.text += "\n Keep moving! Sharks need to swim to breathe.";
		instructionsText.text += "\nPress " + brake + " to start the game.";
		gameOver = false;
		gameStarted = false;
		readyToStart = false;
	}

	void Lose()
	{
		Debug.Log("Player " + this.name + " loses :(");
		instructionsText.text = "You lose :(";
		GameOver();
	}

	public void Win()
	{
		Debug.Log("Player " + this.name + " wins!");
		instructionsText.text = "You win!";
		GameOver();
	}

	void GameOver()
	{
		instructionsText.fontSize = 72;
		instructionsText.text += "\n\nPress " + reset + " to restart.";
		if (audioSource != null)
		{
			audioSource.clip = gameOverClip;
			audioSource.loop = true;
			audioSource.Play();
		}
		gameOver = true;
	}

	void DrawOpponentIndicator()
	{
		Vector3 otherSharkViewportPoint = cam.WorldToViewportPoint(otherPlayer.transform.position);
		// Check if opponent is on screen
		bool otherSharkOnScreen = (
			0 < otherSharkViewportPoint.x && otherSharkViewportPoint.x < 1
			&& 0 < otherSharkViewportPoint.y && otherSharkViewportPoint.y < 1
			&& 0 < otherSharkViewportPoint.z
		);
		// Hide indicator if opponent is on screen
		if (otherSharkOnScreen)
		{
			opponentIndicator.enabled = false;
			return;
		}
		opponentIndicator.enabled = true;

		// Clamp the viewportPoint to the viewport (i.e. 0 to 1)
		if (otherSharkViewportPoint.x < 0)
		{
			otherSharkViewportPoint.x = 0;
			opponentIndicator.transform.rotation = Quaternion.Euler(0f, 0f, -90f);
		}
		else if (otherSharkViewportPoint.x > 1)
		{
			otherSharkViewportPoint.x = 1;
			opponentIndicator.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
		}

		if (otherSharkViewportPoint.y < 0)
		{
			otherSharkViewportPoint.y = 0;
			opponentIndicator.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
		}
		else if (otherSharkViewportPoint.y > 1)
		{
			otherSharkViewportPoint.y = 1;
			opponentIndicator.transform.rotation = Quaternion.Euler(0f, 0f, 180f);
		}

		// Flatten the Vector3 to the camera plane
		otherSharkViewportPoint.z = 0;

		// Scale the viewportPoint to the viewport's pixel size
		otherSharkViewportPoint.x *= cam.pixelWidth;
		otherSharkViewportPoint.y *= cam.pixelHeight;

		// Move the indicator towards the middle of the screen until it's 50px from the edge
		Vector3 towardsViewportCenter = 0.5f * new Vector3(cam.pixelWidth, cam.pixelHeight);
		towardsViewportCenter -= otherSharkViewportPoint;
		towardsViewportCenter.Normalize();

		float scaleFactor = 1;
		if (otherSharkViewportPoint.x == 0 || otherSharkViewportPoint.x == cam.pixelWidth)
		{
			scaleFactor = Mathf.Abs(50 / towardsViewportCenter.x);
		}
		else if (otherSharkViewportPoint.y == 0 || otherSharkViewportPoint.y == -cam.pixelHeight)
		{
			scaleFactor = Mathf.Abs(50 / towardsViewportCenter.y);
		}
		towardsViewportCenter *= scaleFactor;
		otherSharkViewportPoint += towardsViewportCenter;

		if (name == "Shark 1")
		{
			otherSharkViewportPoint.x += cam.pixelWidth;
		}
		opponentIndicator.transform.position = otherSharkViewportPoint;
	}

	void UpdateDeathClock()
	{
		timerText.text = deathTimer.ToString("f2");
	}
}
